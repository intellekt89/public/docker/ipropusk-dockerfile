#!/bin/sh

echo "BUILD_VERSION"
cat ./BUILD_VERSION

echo ${S3_ACCESS_KEY_ID}:${S3_SECRET_ACCESS_KEY} >  ~/.passwd-s3fs
chmod 600  ~/.passwd-s3fs

if [ -z ${PORTAL_CONTENT_DIR+x} ] ; then 
  echo "PORTAL_CONTENT_DIR unset" ;
  export PORTAL_CONTENT_DIR=/app/content ;
fi
echo "PORTAL_CONTENT_DIR ${PORTAL_CONTENT_DIR}"

if [ -z ${S3_BUCKET_CONTENT+x} ] ; then
  echo "S3_BUCKET_CONTENT unset, skip connect to S3 content dir" ;
else
  echo "Connect Yandex cloud Object Storage S3_BUCKET ${S3_BUCKET_CONTENT} with S3_ACCESS_KEY_ID ${S3_ACCESS_KEY_ID}";
  s3fs ${S3_BUCKET_CONTENT} ${PORTAL_CONTENT_DIR} -o passwd_file=$HOME/.passwd-s3fs -o url=http://storage.yandexcloud.net -o use_path_request_style;
fi

echo "PORTAL_CONTENT_DIR:"
ls -lh ${PORTAL_CONTENT_DIR}

if [ -z ${PORTAL_TEMPLATES_DIR+x} ] ; then 
  echo "PORTAL_TEMPLATES_DIR unset"
  export PORTAL_TEMPLATES_DIR=/app/templates ; 
fi
echo "PORTAL_TEMPLATES_DIR ${PORTAL_TEMPLATES_DIR}"


if [ -z ${S3_BUCKET_TEMPLATES+x} ] ; then
  echo "S3_BUCKET_CONTENT unset, skip connect to S3 tempates dir" ;
else
  echo "Connect Yandex cloud Object Storage S3_BUCKET ${S3_BUCKET_TEMPLATES} with S3_ACCESS_KEY_ID ${S3_ACCESS_KEY_ID}"
  s3fs ${S3_BUCKET_TEMPLATES} ${PORTAL_TEMPLATES_DIR} -o passwd_file=$HOME/.passwd-s3fs -o url=http://storage.yandexcloud.net -o use_path_request_style
fi

echo "PORTAL_TEMPLATES_DIR:"
ls -lh ${PORTAL_TEMPLATES_DIR}

ls -lh ${PORTAL_CONTENT_DIR} | grep portal.config ;
if [ -f "${PORTAL_CONTENT_DIR}/portal.config" ]; then
  echo "${PORTAL_CONTENT_DIR}/portal.config exist" ;
else
  echo "${PORTAL_CONTENT_DIR}/portal.config not exist, copy from /app/content-default/portal.config" ;
  ls -lah ${PORTAL_CONTENT_DIR}
  cp /app/content-default/portal.config ${PORTAL_CONTENT_DIR}/portal.config ;
fi
ls -lh ${PORTAL_CONTENT_DIR} | grep portal.config ;

exec "$@"