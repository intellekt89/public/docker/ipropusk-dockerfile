FROM registry.gitlab.com/intellekt89/public/docker/dotnet-aspnet:8.0.11-bookworm-slim-amd64

COPY i-propusk/release /app/bin
COPY i-propusk/content/portal.config /app/content-default/portal.config
ADD --chown=1000:1000 https://gitlab.com/intellekt89/public/docker/ipropusk-dockerfile/-/raw/main/entrypoint.sh /entrypoint.sh
COPY Ipropusk.gd.crt /usr/local/share/ca-certificates/Ipropusk.gd.crt
COPY ca_mail.i-propusk.ru.crt /usr/local/share/ca-certificates/ca_mail.i-propusk.ru.crt

RUN chmod +x /entrypoint.sh && \
    update-ca-certificates

USER 1000

WORKDIR /app/bin
ENTRYPOINT ["/entrypoint.sh"]
CMD ["dotnet", "Intellekt.Web.dll"]





